BEGIN;

CREATE TYPE userRole AS ENUM ('PROFESSOR','STUDENT');
CREATE TYPE userAccessRole AS ENUM ('USER','ADMIN');

-- CREATE TABLE IF NOT EXISTS public."role"
-- (
--     id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
--     name text COLLATE pg_catalog."default"
-- );


-- CREATE TABLE IF NOT EXISTS public."accessRole"
-- (
--     id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
--     name text COLLATE pg_catalog."default"
-- );

CREATE TABLE IF NOT EXISTS public.courses
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name text COLLATE pg_catalog."default",
    begining BIGINT,
    ending BIGINT,
    professor int,
    "group" int,
    "color" text COLLATE pg_catalog."default",
    dates bigint[]

);
CREATE TABLE IF NOT EXISTS public.dayStartPattern
(
    id integer unique NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    starts BIGINT,
    ends BIGINT
);

CREATE TABLE IF NOT EXISTS public.pattern
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    dayCount int,
    dayStartPattern int REFERENCES dayStartPattern (id)
);


insert into dayStartPattern("starts", "ends") values(1653296400000 - 10800000 , 1653301200000 -10800000);
insert into dayStartPattern("starts", "ends") values(1653301800000 -10800000 , 1653306600000-10800000);
insert into dayStartPattern("starts", "ends") values(1653307800000 -10800000, 1653312600000-10800000);
insert into dayStartPattern("starts", "ends") values(1653313200000 -10800000, 1653318000000-10800000);
insert into dayStartPattern("starts", "ends") values(1653318600000 -10800000, 1653323400000-10800000);
insert into dayStartPattern("starts", "ends") values(1653324000000 -10800000, 1653328800000-10800000);
insert into dayStartPattern("starts", "ends") values(1653329400000 -10800000, 1653334200000-10800000);
insert into dayStartPattern("starts", "ends") values(1653334800000 -10800000, 1653339600000-10800000);

CREATE TABLE IF NOT EXISTS public."group"
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name text COLLATE pg_catalog."default"
);

CREATE TABLE IF NOT EXISTS public.schedule
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    "course" integer,
    "group" integer,
    "startTime" BIGINT,
    "professor" integer,
    "professorT" text COLLATE pg_catalog."default",
    "color" text COLLATE pg_catalog."default",
    "slot" integer
);

CREATE TABLE IF NOT EXISTS public.sessions
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    "accessToken" text COLLATE pg_catalog."default",
    "refreshToken" text COLLATE pg_catalog."default"
);

CREATE TABLE IF NOT EXISTS public.users
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    "group" integer,
    name text COLLATE pg_catalog."default",
    "email" text COLLATE pg_catalog."default",
    role userRole,
    "password" text,
    accessRole userAccessRole
    
);

insert into pattern ("daycount", "daystartpattern") values(1,1);
insert into pattern("daycount", "daystartpattern") values(1,2);
insert into pattern("daycount", "daystartpattern") values(1,3);
insert into pattern("daycount", "daystartpattern") values(1,4);
insert into pattern("daycount", "daystartpattern") values(1,5);
insert into pattern("daycount", "daystartpattern") values(1,6);
insert into pattern("daycount", "daystartpattern") values(1,7);
insert into pattern("daycount", "daystartpattern") values(1,8);

insert into pattern("daycount", "daystartpattern") values(2,1);
insert into pattern("daycount", "daystartpattern") values(2,2);
insert into pattern("daycount", "daystartpattern") values(2,3);
insert into pattern("daycount", "daystartpattern") values(2,4);
insert into pattern("daycount", "daystartpattern") values(2,5);
insert into pattern("daycount", "daystartpattern") values(2,6);
insert into pattern("daycount", "daystartpattern") values(2,7);
insert into pattern("daycount", "daystartpattern") values(2,8);

insert into pattern("daycount", "daystartpattern") values(3,1);
insert into pattern("daycount", "daystartpattern") values(3,2);
insert into pattern("daycount", "daystartpattern") values(3,3);
insert into pattern("daycount", "daystartpattern") values(3,4);
insert into pattern("daycount", "daystartpattern") values(3,5);
insert into pattern("daycount", "daystartpattern") values(3,6);
insert into pattern("daycount", "daystartpattern") values(3,7);
insert into pattern("daycount", "daystartpattern") values(3,8);

insert into pattern("daycount", "daystartpattern") values(4,1);
insert into pattern("daycount", "daystartpattern") values(4,2);
insert into pattern("daycount", "daystartpattern") values(4,3);
insert into pattern("daycount", "daystartpattern") values(4,4);
insert into pattern("daycount", "daystartpattern") values(4,5);
insert into pattern("daycount", "daystartpattern") values(4,6);
insert into pattern("daycount", "daystartpattern") values(4,7);
insert into pattern("daycount", "daystartpattern") values(4,8);

insert into pattern("daycount", "daystartpattern") values(5,1);
insert into pattern("daycount", "daystartpattern") values(5,2);
insert into pattern("daycount", "daystartpattern") values(5,3);
insert into pattern("daycount", "daystartpattern") values(5,4);
insert into pattern("daycount", "daystartpattern") values(5,5);
insert into pattern("daycount", "daystartpattern") values(5,6);
insert into pattern("daycount", "daystartpattern") values(5,7);
insert into pattern("daycount", "daystartpattern") values(5,8);

insert into pattern("daycount", "daystartpattern") values(6,1);
insert into pattern("daycount", "daystartpattern") values(6,2);
insert into pattern("daycount", "daystartpattern") values(6,3);
insert into pattern("daycount", "daystartpattern") values(6,4);
insert into pattern("daycount", "daystartpattern") values(6,5);
insert into pattern("daycount", "daystartpattern") values(6,6);
insert into pattern("daycount", "daystartpattern") values(6,7);
insert into pattern("daycount", "daystartpattern") values(6,8);


insert into pattern("daycount", "daystartpattern") values(8,1);
insert into pattern("daycount", "daystartpattern") values(8,2);
insert into pattern("daycount", "daystartpattern") values(8,3);
insert into pattern("daycount", "daystartpattern") values(8,4);
insert into pattern("daycount", "daystartpattern") values(8,5);
insert into pattern("daycount", "daystartpattern") values(8,6);
insert into pattern("daycount", "daystartpattern") values(8,7);
insert into pattern("daycount", "daystartpattern") values(8,8);

insert into pattern("daycount", "daystartpattern") values(9,1);
insert into pattern("daycount", "daystartpattern") values(9,2);
insert into pattern("daycount", "daystartpattern") values(9,3);
insert into pattern("daycount", "daystartpattern") values(9,4);
insert into pattern("daycount", "daystartpattern") values(9,5);
insert into pattern("daycount", "daystartpattern") values(9,6);
insert into pattern("daycount", "daystartpattern") values(9,7);
insert into pattern("daycount", "daystartpattern") values(9,8);

insert into pattern("daycount", "daystartpattern") values(10,1);
insert into pattern("daycount", "daystartpattern") values(10,2);
insert into pattern("daycount", "daystartpattern") values(10,3);
insert into pattern("daycount", "daystartpattern") values(10,4);
insert into pattern("daycount", "daystartpattern") values(10,5);
insert into pattern("daycount", "daystartpattern") values(10,6);
insert into pattern("daycount", "daystartpattern") values(10,7);
insert into pattern("daycount", "daystartpattern") values(10,8);

insert into pattern("daycount", "daystartpattern") values(11,1);
insert into pattern("daycount", "daystartpattern") values(11,2);
insert into pattern("daycount", "daystartpattern") values(11,3);
insert into pattern("daycount", "daystartpattern") values(11,4);
insert into pattern("daycount", "daystartpattern") values(11,5);
insert into pattern("daycount", "daystartpattern") values(11,6);
insert into pattern("daycount", "daystartpattern") values(11,7);
insert into pattern("daycount", "daystartpattern") values(11,8);

insert into pattern("daycount", "daystartpattern") values(12,1);
insert into pattern("daycount", "daystartpattern") values(12,2);
insert into pattern("daycount", "daystartpattern") values(12,3);
insert into pattern("daycount", "daystartpattern") values(12,4);
insert into pattern("daycount", "daystartpattern") values(12,5);
insert into pattern("daycount", "daystartpattern") values(12,6);
insert into pattern("daycount", "daystartpattern") values(12,7);
insert into pattern("daycount", "daystartpattern") values(12,8);

insert into pattern("daycount", "daystartpattern") values(13,1);
insert into pattern("daycount", "daystartpattern") values(13,2);
insert into pattern("daycount", "daystartpattern") values(13,3);
insert into pattern("daycount", "daystartpattern") values(13,4);
insert into pattern("daycount", "daystartpattern") values(13,5);
insert into pattern("daycount", "daystartpattern") values(13,6);
insert into pattern("daycount", "daystartpattern") values(13,7);
insert into pattern("daycount", "daystartpattern") values(13,8);

COMMENT ON TABLE public.users
    IS 'table for all users';
END;