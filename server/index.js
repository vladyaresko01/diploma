const model = require("./model");
const keys = require("./keys");
const user = require("./methods/user");
const schedule = require("./jsonTemplates/groups/schedule");

// Express Application setup
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

let colors = [
  "#f8bbd0",
  "#e1bee7",
  "#b2dfdb",
  "#ffecb3",
  "#ffccbc",
  "#f75990",
  "#f75990",
  "#00DDFF",
  "#0049B7",
  "#a28089",
  "#e5eaf5",
  "#d0bdf4",
  "#e5eaf5",
  "#8458B3",
];

const app = express();
app.use(cors());
app.use(bodyParser.json());

// Postgres client setup
const { Pool } = require("pg");
const { query } = require("express");
const e = require("express");
const pgClient = new Pool({
  user: keys.pgUser,
  host: keys.pgHost,
  database: keys.pgDatabase,
  password: keys.pgPassword,
  port: keys.pgPort,
});

/** Error Handler */
async function errorHandler(res, func) {
  try {
    res = await func(res);
  } catch (err) {
    console.log(err);
    res.status(500);
    res.send();
  }
}

///Express route definitions
app.get("/test", (req, res) => {
  res.send("Hi");
});

//
// users methods start
//

app.get(`/user/:id`, async (req, res) => {
  await errorHandler(res, async () => {
    const temp = await pgClient.query(
      `SELECT * FROM users WHERE id=${req.params.id}`
    );
    res.send(temp.rows);
  });
});

app.post(`/user`, async (req, res) => {
  await errorHandler(res, async () => {
    let temp = req.body;
    console.log(temp);
    let str = "";
    if ("id" in temp) {
      str = `UPDATE users SET "group" = ${temp.group}, "email" = '${temp.email}', "name" = '${temp.name}', "role" = '${temp.role}', "accessrole" = '${temp.accessrole}', "password" = '${temp.password}' WHERE id = ${temp.id}`;
    } else {
      str = `insert into users("group","email","name", "role", "accessrole","password") values (${temp.group},'${temp.email}','${temp.name}','${temp.role}','${temp.accessrole}','${temp.password}')`;
    }
    console.log(str);
    let result = await pgClient.query(str);
    res.send("yes");
  });
});

app.get(`/group/:id`, async (req, res) => {
  await errorHandler(res, async () => {
    const temp = await pgClient.query(
      `SELECT * FROM users WHERE "group"=${req.params.id}`
    );
    res.send(temp.rows);
  });
});

//
// scedule methods start
//insert into schedule("course", "group", "startTime","professor") values (1,41,1653550200000,8)

app.get(`/scedule`, async (req, res) => {
  await errorHandler(res, async () => {
    let temp = [];
    let scedule = 0;
    if (req.query.id) {
      scedule = await pgClient.query(
        `SELECT  row_to_json(users) as user, row_to_json(courses) as courseData ,schedule.* from schedule JOIN users on (users.id = schedule.professor) JOIN courses on (courses.id = schedule.course)  where schedule.professor = ${req.query.id}`
      );
      let slots = await pgClient.query(
        `SELECT  slot FROM schedule where professor =  ${req.query.id}`
      );
      // slots = slots["rows"];
      // slots = slots.map((a) => a.slot);
      // console.log(slots);
      // for (let i = 1; i < 85; i++) {
      //   if (!slots.includes(i)) {
      //     temp.push(i);
      //   }
      // }
      // fillSceduleEmpty(temp,scedule.rows)
    } 
    
    else if (req.query.group) {
      scedule = await pgClient.query(
        `SELECT  row_to_json(users) as user, row_to_json(courses) as courseData ,schedule.* from schedule JOIN users on (users.id = schedule.professor) JOIN courses on (courses.id = schedule.course)  where schedule.group = ${req.query.group}`
      );
    }
    scedule.rows.map((el) => {
      delete el.course;
      delete el.professor;
    });
    console.log(req.query);
    res.send(scedule.rows);
  });
});

app.post(`/scedule`, async (req, res) => {
  //create new scedule element or change exiting

  await errorHandler(res, async () => {
    let temp = req.body;
    let str = "";
    if ("id" in temp) {
      str = `UPDATE schedule SET "course" = ${temp.course}, "group" = '${temp.group}', "startTime" = '${temp.startTime}', "professor" = '${temp.professor}' WHERE id = ${temp.id}`;
    } else {
      str = `insert into schedule("course","group","startTime", "professor") values (${temp.course},'${temp.group}','${temp.startTime}','${temp.professor}')`;
    }
    console.log(str);
    let result = await pgClient.query(str);
    res.send("yes");
  });
});

// get course name
app.get(`/course/:id`, async (req, res) => {
  await errorHandler(res, async () => {
    const temp = await pgClient.query(
      `SELECT * FROM courses WHERE id=${req.params.id}`
    );
    res.send(temp.rows);
  });
});
app.get(`/course`, async (req, res) => {
  await errorHandler(res, async () => {
    // get all courses
    let temp = [];
    if (req.query.professor) {
      temp = await pgClient.query(
        `SELECT * FROM courses WHERE "name" = ${req.query.professor}`
      );
    } else {
      temp = await pgClient.query(`SELECT * FROM courses`);
    }

    res.send(temp["rows"]);
  });
});

app.post(`/course`, async (req, res) => {
  await errorHandler(res, async () => {
    let body = req.body;
    //86400000 - 24 hours milies
    let temp = await pgClient.query(`SELECT "color" FROM courses`);
    temp = temp["rows"];
    let temp2 = temp.map(({ color }) => color);
    let uniqueColor = colors.filter((el) => !temp2.includes(el));

    let course = [];
    if ("id" in body) {
      await pgClient.query(`delete from schedule where "course" = ${body.id}`);
      course = await pgClient.query(
        `UPDATE courses SET "name" = '${body.name}', "begining" = ${body.begining}, "ending" = ${body.ending},"group" = ${body.group}, "dates" = ARRAY [${body.dates}], "professor" = ${body.professor} WHERE id = ${body.id} RETURNING *`
      );
    } else {
      course = await pgClient.query(
        `insert into "courses" ("name","begining","ending","professor","dates","group","color") values ('${body["name"]}',${body.begining},${body.ending},${body.professor}, ARRAY [${body.dates}],${body.group},'${uniqueColor[0]}') RETURNING *`
      );
    }

    createScedule(course.rows[0]);
    res.send(course.rows[0]);
  });
});

app.post(`/course/delete/:id`, async (req, res) => {
  await errorHandler(res, async () => {
    // delete course by id and all scedules
    await pgClient.query(`delete from courses where "id" = ${req.params.id}`);
    await pgClient.query(
      `delete from schedule where "course" = ${req.params.id}`
    );
    res.send();
  });
});

async function createScedule(courseRow) {
  // function for creating scedule rows from course row
  let pattern = await pgClient.query("select * from pattern");
  pattern = pattern.rows;
  let dayPattern = await pgClient.query("select * from daystartpattern");
  dayPattern = dayPattern.rows;
  let currentsceduleTime = courseRow.startTime;

  let i = parseInt(courseRow.begining),
    j = 0;
  while (i <= parseInt(courseRow.ending)) {
    {
      console.log("iteration" + j);
      courseRow.dates.forEach((element) => {
        console.log(element);
        let currentDate = new Date(parseInt(i));
        let day = parseInt(pattern[element - 1].daycount);
        let time = new Date(
          parseInt(dayPattern[pattern[element - 1].daystartpattern - 1].starts)
        );

        let date = new Date(currentDate.getTime() + 86400000 * (day - 1));
        date.setHours(time.getHours());
        date.setMinutes(time.getMinutes());
        let str = `insert into schedule("course","group","startTime","professor","color","slot") values (${
          courseRow.id
        },${courseRow.group},${date.getTime()},${courseRow.professor},'${
          courseRow.color
        }',${element})`;

        pgClient.query(str);
      });
    }
    (i = i + 1209600000), j++;
  }
}

/** User login */
app.post("/login", async (req, res) => {
  await errorHandler(res, async () => {
    let body = req.body;
    body;
    let user = [];
    console.log(body);
    user = await pgClient.query(
      `select * from users where "email" = '${body.email}' AND "password" = '${body.password}'`
    );
    user = user.rows[0];
    delete user.password;
    if (!user) {
      res.status(401);
    }
    res.send(user);
    // res.send(req.body);
  });
});

app.get("/createcourse",async(req,res)=>{
  await errorHandler(res,async()=>{
    let body = req.body;
    let startTime = req.query.startTime
    let endTime = req.query.endTime
    let professor = req.query.professor;
    let group = req.query.group
    let ans = await pgClient.query(`SELECT row_to_json(users) as user, row_to_json(courses) as courseData ,schedule.* FROM schedule JOIN users on (users.id = schedule.professor) JOIN courses on (courses.id = schedule.course) where "startTime">=${startTime} and "startTime"<${endTime}  and  (schedule.professor = ${professor} or ( schedule.professor = -1 and slot not in (select "slot" from schedule where schedule.professor = ${professor})and slot not in (select "slot" from schedule where "group" = ${group} and professor!=${professor}) ) or schedule.group = ${group}) order by "slot" `)
    res.send(ans["rows"]);
  })
})
app.get("/allGroups",async(req,res)=>{
  await errorHandler(res,async()=>{
    let ans = await pgClient.query(`select distinct "group" from users where"group" != -1`)
    res.send(ans["rows"])
  })
})
app.get("/allslot",async(req,res)=>{
  await errorHandler(res,async()=>{
    let professor = req.query.professor;
    let group = req.query.group
    let ans = await pgClient.query(`select distinct "slot" from schedule where "group" =${group} or "professor" = ${professor}`)
    res.send(ans["rows"])
  })
})

app.listen(5000, (err) => {
  console.log("Listening");
});
console.log("test");
