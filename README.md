# diploma
## Learning managment system made with React, Node.js, Postgres, Nginx, puted into Docker containers
## Features
- import/edit/delete users
- import/edit/delete courses
- autogenerated scedule when course created/edited
- used docker containers

## Tech
- [React](https://reactjs.org/) - for creating front-end
- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework
- [postgres]-
- [ngnix]-
- [docker]-
## Installation
Install the dependencies and devDependencies and start the server.

install @ start server
```sh
cd server
npm i
node index.js
```

install @ start client
```sh
cd client
npm i
npm start
```

install and deploy in a Docker container.

```sh
docker-compose up --build
```


## vlad Iaresko IPZ-41 diploma work

