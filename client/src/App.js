import { Fragment } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Link,Routes, useNavigate } from "react-router-dom";
import OtherPage from "./components/OtherPage/OtherPage";
import Error500 from "./components/500/Error500"
import './components/styles/themes.scss'
import Modal from "./components/Modal/Modal";
import Scedule from "./components/scedule/Scedule";
import React, { useEffect, useState } from "react";

export default function App() {

  function logout(){
    sessionStorage.setItem('user','{"name" : null}');
    changeUser({name:null})
    setisLoggedIn(false)
  }


  

  if (sessionStorage.getItem("user") === null) {
    sessionStorage.setItem('user','{"name" : null}');
  }




  function userHandler(obj,url){
    sessionStorage.setItem('user', JSON.stringify(obj));
    changeUser(obj)
    window.location.href = '/user';
  }
  const [user,changeUser] = useState(JSON.parse(sessionStorage.getItem("user")));
  const [isLoggedIn,setisLoggedIn] = useState(false);
  useEffect(()=>{
    if(user.name == null){
      setisLoggedIn(false)
    }
    else{
      setisLoggedIn(true)
    }
    console.log(user)
  },[user])
  return (
    <Router>
      <Fragment>
        <header className="header">
          <Link className="button" to="/">Home</Link>
          {isLoggedIn?(<><Link className="button" to="/user">my Page</Link><Link className="button" onClick = {logout}to="/">Logout</Link></>):(<>

          <Link className="button" to="/login">Login</Link>
          <Link className="button" to="/Registration">Registration</Link>
          </>

          )}

        </header>
        <div className="background">
        <Routes classname="background">
          <Route className="header" path="/">
            <Route className="header" path = "login" element={<Modal func = {userHandler} type = 'login'/>}></Route>
            <Route  className="header" path="Registration"  element={<Modal func = {userHandler} type = {'registration'}/>} />
          </Route>
          <Route  className="header" path="/user"  element={<Scedule user = {user}/>} />          
        </Routes>
        </div>
        
        <footer className="footer">
          made by Vlad Iaresko IPZ-41
        </footer>
      </Fragment>
    </Router>
  );
}

// export default App;
