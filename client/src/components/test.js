"";
import Paper from "@mui/material/Paper";
import React, { useEffect, useState } from "react";
import { ViewState } from "@devexpress/dx-react-scheduler";
import CreateCourse from "./CreateCourse/CreateCourse";

import   {Resources,  Scheduler,  DayView, Appointment,  WeekView,  Appointments,  AppointmentTooltip,  AppointmentForm}  from "@devexpress/dx-react-scheduler-material-ui";
import axios from "axios";



export default function Test(props) {
  const [selected,setSelected] = useState([])
  // var scheduler = $(".scheduler").dxScheduler({
	// 	onAppointmentClick:function(e){
  //   	var str = '';
  //   	for(var key in e.appointmentData)
  //     	str+= key + ': ' + e.appointmentData[key] + '; ';
  //   	alert(str);
  //   },}).dxScheduler("instance");
  const [isCustom,setIsCustom] = useState(true)
  const [groups,setGroups] = useState()
  const [currentGroup, setCurrentGroup] = useState()
  const[form,setForm] = useState()

  function setVisible(isVisible){
    setIsCustom(!isVisible)
  }

  function formHandler(data){
    data.preventDefault();
    setForm(data)
  }
  function getForm(passedForm){
    console.log(passedForm)
    console.log(selected)
    let startCourse = new Date(passedForm.start)
    startCourse = startCourse.getTime()
    let endCourse = new Date(passedForm.end)
    endCourse = endCourse.getTime()
    let body = {
      name:passedForm.course,
      begining:startCourse,
      ending:endCourse,
      professor:props.userid,
      dates:selected,
      group: parseInt(passedForm.select)
    }
    axios.post("api/course",body).then((res) => {
      console.log(res.data)
      props.groupHandler(undefined);
      setSelected([])
    });
  }
  function clickHandler(ev,el){
    el.data.color = "#b0b0b0"
    let value = parseFloat(ev)
    let tempArray = selected
    let ocupied = []
    props.ocupied.map(e=>{
      ocupied.push(e.slot)
    })
    if(!ocupied.includes(value)){
      props.handleChangeColor(value,currentGroup)

      if(tempArray.includes(value)){
        tempArray = tempArray.filter(e => {return e !== value})
        console.log(tempArray)
        setSelected(tempArray) 
      }
      else{
        tempArray.push(value)
        setSelected(tempArray)
      }
    }
    
  }
  if(!groups){
    axios.get("/api/allGroups").then(res=>{
      setGroups(res.data)
    })
  }

  function changeGroup(data){props.groupHandler(data)}
  let tempScedule = props.scedule;

  if(currentGroup){
    let startDate = Date.now()-604800000
    // tempScedule = axios.get("/api/allGroups")
  }

  
  


  const currentDate = new Date(Date.now() + 604800000);
  let app = [{}];
  const resourcesData = [{
    id:"#b0b0b0",
    color:"#b0b0b0",
    text:`Lecturer`,

  }];
  const groupData = [{
    id:"#b0b0b0",
    group:"#b0b0b0",
    text:`Group`
  }];
  let resources = [
    {
      fieldName: "color",
      instances: resourcesData,
    },
    {
      fieldName: "group",
      title: "group",
      instances: groupData,
    }
  ];


  if (tempScedule !== undefined) {
    let temp = [];
    tempScedule.map((element) => {
      let title = `group: ${element.group} ${element.coursedata.name}`
      let group = element.group
      if (props.user === "STUDENT"){
        title = `${element.coursedata.name}`
      }
      if(element.group === -1){
        title = 'free'
        group = 'free'
      }
      temp.push({
        title:title,
        startDate: new Date(parseInt(element.startTime)),
        endDate: new Date(parseInt(element.startTime) + 4800000),
        id: element.id,
        color: element.color,
        group: element.group,
        slot:element.slot
      });
      resourcesData.push({
        id:element.color,
        color:element.color,
        text:`Lecturer: ${element.user.name}`,

      })
      groupData.push({
        id:element.group,
        group:element.group,
        text:`Group: ${group}`
      })
    });
    app = temp;


  }
  // const AppointmentContent={
  //   return(<Appointments.AppointmentContent
  //     style={
  //       color: "#000000"
  //     }
  //   />)
  // }
  const Appointment = ({
    children, style, ...restProps
  }) => (
    <Appointments.Appointment
      {...restProps}
      style={{
        ...style,
      }}
      onClick = {(event)=>clickHandler(restProps.data.slot,event)}
    >
      {children}
    </Appointments.Appointment>
  );



  return (
    <Paper>
      {props.user === "PROFESSOR"?(<CreateCourse selected = {selected} setVisible = {setVisible} getForm={getForm} groups ={groups} changeGroup = {changeGroup} ocupied = {[1,3,5]}></CreateCourse>):(<></>)}
      <Scheduler id="scheduler1" data={app} height={700} firstDayOfWeek ={1} onClick={() => {
            console.log(1);
          }}>
        <WeekView startDayHour={9} endDayHour={21} cellDuration={60} />   
        {isCustom? (<Appointments/>):(<Appointments  appointmentComponent={Appointment}/>)}
        <AppointmentTooltip/>
        <AppointmentForm onClick={clickHandler}/>
        <Resources data={resources}  mainResourceName="color" />
      </Scheduler>
      <Scheduler id="scheduler2" onAppointmentClick={() => {
            console.log(1);
          }} firstDayOfWeek ={1} data={app} height={700}>
        <ViewState onclick = {clickHandler}
          currentDate={currentDate}
          mainResourceName="roomId"
        />
        <WeekView startDayHour={9} endDayHour={21} cellDuration={60}/>

        {isCustom? (<Appointments/>):(<Appointments  appointmentComponent={Appointment}/>)}
        <AppointmentTooltip/>
        <AppointmentForm onClick={clickHandler}/>
        <Resources data={resources}  mainResourceName="color" />
        <Resources data={resources} mainResourceName="color" />
        <AppointmentTooltip />
      </Scheduler>
    </Paper>
  );
  
};

