import React from 'react';
import './Error500.scss';
import deathStar from './death-star.jpeg';
import { Link } from "react-router-dom";

const Error500 = () => {


  return (
    <div className='error-500'>
      <div><img src={deathStar} alt='death-star'></img></div>
      <h3>An error has occurred</h3>
      <h4>But we already sent droids to fix it</h4>
      <div><Link to="/">Home</Link></div>
    </div>
  )
}

export default Error500