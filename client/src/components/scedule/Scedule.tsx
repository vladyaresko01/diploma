import axios from "axios";
import React, { useEffect, useState } from "react";
import Paper from '@mui/material/Paper';
import { Scheduler, View } from 'devextreme-react/scheduler';
import Test from '../test'
import { ColorBox } from "devextreme-react";

type Props = {
  user:{
    id:number
    name:string
    group:bigint
    role:string
  }
}
type Schedule={
  color:string,
  coursedata:Object,
  group:Number,
  id:number,
  professor:number,
  professorT:string,
  slot:number,
  startTime:string,
  user:Object
}


export default function Scedule({user}: Props) {


  const [scedule,setScedule] = useState<Array<Schedule>>()
  const [group,setGroup] = useState<Number>()
  const [ocupied,setOcupied]= useState<Array<Object>>()

  function groupHandler(num:Number){
    console.log(num);
    setGroup(num)
  }
  function getData(){

      if(user.role == "STUDENT"){
        axios.get(`/api/scedule?group=${user.group}`).then(res=>{
          console.log(1);
         setScedule(res.data)
        })
      }
      else if(user.role == "PROFESSOR"){
        axios.get(`/api/scedule?id=${user.id}`).then(res=>{
          setScedule(res.data)
          console.log(res.data);
        })
      }

  }
  function handleChangeColor(slot:number){
 
    if(scedule!==undefined){
      let tempCourse = scedule.map(element =>{
        if(element.slot === slot){
          if(element.color === "#f0f0f0"){
            element.color = "#b0b0b0"
            if(group!==undefined){
              element.group = group
            }
          }
          else if(element.color === "#b0b0b0"){
            element.color = "#f0f0f0"
            element.group = -1
          }
        }

        return element
      })
      setScedule(tempCourse)
    }

  }

  useEffect(()=>{
     if (group === undefined){getData()}
    else if(group!==undefined && user.role == "PROFESSOR"){
      axios.get(`/api/allslot?group=${group}&professor=${user.id}`).then(res=>{
        setOcupied(res.data)
      })
      const today = new Date();
      const firstDay = new Date(today.setDate(today.getDate() - today.getDay()));
      const lastDay = new Date(today.setDate(today.getDate() - today.getDay() + 14));
      axios.get(`/api/createcourse?professor=${user.id}&group=${group}&endTime=${lastDay.getTime()}&startTime=${firstDay.getTime()}`).then(res=>{
        setScedule(res.data)
      })
    }
  },[group])

  

  return (
    <Test groupHandler={groupHandler} handleChangeColor={handleChangeColor} scedule = {scedule} ocupied = {ocupied} userid={user.id} user={user.role}/>
  )
}