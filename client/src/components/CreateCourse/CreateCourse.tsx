import React, { useEffect, useState } from "react";
import axios from "axios";
import "./CreateCourse.scss";
import "../styles/themes.scss";
import { Group } from "devextreme-react/diagram";
import { GroupingState } from "@devexpress/dx-react-scheduler";
type Props = {
  changeGroup: Function;
  groups: Array<Groups>;
  ocupied:Array<number>;
  setVisible: Function;
  getForm:Function;
};
type Groups = {
  group: number;
};

export default function CreateCourse({ changeGroup, groups, ocupied,setVisible,getForm}: Props) {
  const [isVisible,setIsVisible]= useState<Boolean>(false)

  let checkbox = ([<></>]);
  if(ocupied !== undefined){
    for(let i =1;i<105;i++){
      if(!ocupied.includes(i)){
        checkbox.push(<input name="checkbox" value={i} type="checkbox"></input>)
      }
      else{
        checkbox.push(<input name="checkbox" value={i} type="checkbox" disabled></input>)
      }
    }
  }

  function submitHandler(event:any){
    event.preventDefault();
    if(isVisible){
      getForm(form)
    }
    setIsVisible(!isVisible)
    setVisible(!isVisible)    

  }
  type Form = {
    course: string;
    select: number;
    start: BigInt;
    end: BigInt;
    checkbox:Array<number>
  };
  const [form, setForm] = useState<Form>({
    course: "",
    select: 0,
    start: BigInt(0),
    end: BigInt(0),
    checkbox:[]
  });
  const handleChange = (event: any) => {
    if(event.target.name !== "checkbox"){
      event.preventDefault();
      if (event.target.name === "select") {
        changeGroup(event.target.value);
      }
      setForm({ ...form, [event.target.name]: event.target.value });

    }
    else if (event.target.name === "checkbox"){
      let value = parseFloat(event.target.value)
      let tempArray = form.checkbox
      if(form.checkbox.includes(value)){
        tempArray= tempArray.filter(e => { return e !== value })
        setForm({...form, checkbox : tempArray})
      }
      else{
        tempArray.push(value)
        setForm({...form, checkbox : tempArray})
      }
    }
  };

  let options = [<></>];
  if (groups !== undefined) {
    options = groups.map((el) => (
      <option key={el.group} value={el.group}>
        {" "}
        group: {el.group}
      </option>
    ));
  }
  return (
    <div className="form-container">
      <div className="button submit"  onClick={submitHandler}>Create Course</div>
      {isVisible ?(
        <form id="form" className="form" onChange={handleChange}>
        <select name="select"><option>select</option>{options}</select>
        <input className="form-element" name="course" type="text" />
        <input className="form-element" name="start" type="date"></input>
        <input className="form-element" name="end" type="date"></input>
        {/* <div className = "checkBox-contaiber">
        {checkbox}
        </div> */}

      </form>):(<></>)}
           
    </div>
  );
}
