import React, { useEffect, useState } from "react";


type Props = {passedFunction:Function};
type LoginScreen = {
  email:string,
  password:string
}
export default function Login({passedFunction}: Props) {
  const url = "api/login"

  const [login, setLogin] = useState<LoginScreen>({
    email:'',
    password:''
  });


  useEffect(() => {
    passedFunction(login,url)
  }, [login]);

  const handleChange = (event: any) => {
    let temp = event.currentTarget;
    
    event.preventDefault();
    setLogin({...login,[temp.type]:temp.value});
  };
  return (
    <form className="login-form">
      <label className="label">Email</label>
      <input className="input" type="email" onChange={handleChange} />
      <label className="label">password</label>
      <input className="input" type="password" onChange={handleChange} />
    </form>
  );
}
