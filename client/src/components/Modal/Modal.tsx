import React, { useEffect, useState } from "react";
import "./modal.scss";
import { Link } from "react-router-dom";
import Login from "./Login";
import Registration from "./Registration";
import axios from "axios";
import { couldStartTrivia } from "typescript";

type Props = {
  type: string;
  func: Function;
};

export default function Modal({ type, func }: Props) {
  const [body, setform] = useState <Object>({});
  const[currentUrl,setCurrentUrl] = useState <string>("")
  

  function submitForm() {
    axios.post(currentUrl,body).then((res) => {
      func(res.data)
    });
  }


  function setData(data: object, url:string) {
    setform(data);
    setCurrentUrl(url)
  }
  function getUser() {}

  return (
    <div className="modal">
      <div className="modal-links">
        <Link className="button" to="/login">
          Login
        </Link>
        <Link className="button" to="/Registration">
          Registration
        </Link>
      </div>
      {(() => {
        switch (type) {
          case "login":
            return <Login passedFunction={setData} />;
          case "registration":
            return <Registration passedFunction={setData} />;
        }
      })()}
      <button onClick={() => submitForm()} className="button">
        {type}
      </button>
    </div>
  );
}
