import React, { useState,useEffect } from 'react'
import './modal.scss'
type Props = {passedFunction:Function}

export default function Registration({passedFunction}: Props) {
  const url = "api/users"
  const [form,setform] = useState({
    firstname:'',
    secondname:'',
    email:'',
    password:'',
    passwordConfirm:'',
  })

  const handleChange = (event: any) => {
    let temp = event.currentTarget;
    event.preventDefault();
    console.log(temp.value);
    console.log(temp.name);
    setform({ ...form, [temp.name]: temp.value });
  };

  useEffect(() => {
    passedFunction(form,url);
  }, [form]);
  return (
    <form className='login-form'>
    <label className='label'>Firstname</label>
    <input className='input'name='firstname' onChange={handleChange} type='firstname'/>
    <label className='label'>Secondname</label>
    <input className='input'name='secondname' onChange={handleChange} type='secondname'/>
    <label className='label'>Email</label>
    <input className='input' name='email' onChange={handleChange} type='email'/>
    <label className='label'>password</label>
    <input className='input'name="password" onChange={handleChange} type='password'/>
    <label className='label'>re-enter password</label>
    <input className='input'name="passwordConfirm" onChange={handleChange} type='password'/>
</form>
  )
}